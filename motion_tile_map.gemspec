# -*- encoding: utf-8 -*-
VERSION = "1.0"

Gem::Specification.new do |spec|
  spec.name          = "motion_tile_map"
  spec.version       = VERSION
  spec.authors       = ["Sean Scally"]
  spec.email         = ["sean.scally@gmail.com"]
  spec.description   = %q{port of JSTileMap to RubyMotion}
  spec.summary       = %q{port of JSTileMap to RubyMotion}
  spec.homepage      = ""
  spec.license       = ""

  files = []
  files << 'README.md'
  files.concat(Dir.glob('lib/**/*.rb'))
  spec.files         = files
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "rake"
end
