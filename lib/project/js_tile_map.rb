class JSTileMap < SKNode
  # TODO: Duplicated
  ORIENTATION_ORTHOGONAL = 0
  ORIENTATION_ISOMETRIC = 1

  LAYER_ATTRIBUTE_NONE = 1 << 0
  LAYER_ATTRIBUTE_BASE_64 = 1 << 1
  LAYER_ATTRIBUTE_GZIP = 1 << 2
  LAYER_ATTRIBUTE_ZLIB = 1 << 3

  PROPERTY_NONE = 0
  PROPERTY_MAP = 1
  PROPERTY_LAYER = 2
  PROPERTY_OBJECT_GROUP = 3
  PROPERTY_OBJECT = 4
  PROPERTY_TILE = 5
  PROPERTY_IMAGE_LAYER = 6

  attr_accessor :mapSize, :tileSize, :parentElement, :parentGID, :orientation, :filename, :resources,
    :tilesets, :tileProperties, :properties, :layers, :imageLayers, :objectGroups, :gidData, :zOrderCount,
    :currentString, :storingCharacters, :currentFirstGID, :layerAttributes

  def self.mapNamed mapName
    mapNamed mapName, withBaseZPosition: 0.0, andZOrderModifier: -20.0
  end

  def self.mapNamed mapName, withBaseZPosition: baseZPosition, andZOrderModifier: zOrderModifier

    # zOrder offset.  Make this bigger if you want more space between layers.
    # higher numbers act further away.

    # create the map
    map = alloc.init

    # get the TMX map filename
    name = mapName
    extension = nil

    # split the extension off if there is one passed
    if mapName.rangeOfString(".").location != NSNotFound
      name = mapName.stringByDeletingPathExtension
      extension = mapName.pathExtension
    end

    # load the TMX map from disk
    path = NSBundle.mainBundle.pathForResource name, ofType: extension
    mapData = NSData.dataWithContentsOfFile path

    # set the filename
    map.filename = path

    # parse the map
    parser = NSXMLParser.alloc.initWithData mapData
    parser.delegate = map
    parser.shouldProcessNamespaces = false
    parser.shouldReportNamespacePrefixes = false
    parser.shouldResolveExternalEntities = false
    parsed = parser.parse
    unless parsed
      NSLog "Error parsing map! #{parser.parserError}"
      return nil
    end

    # set zPosition range
    # if baseZPosition < (baseZPosition + (zOrderModifier * (map.zOrderCount + 1)))
    #   map.minZPositioning = baseZPosition
    #   map.maxZPositioning = baseZPosition + (zOrderModifier * (map.zOrderCount + 1))
    # else
    #   map.maxZPositioning = baseZPosition
    #   map.minZPositioning = baseZPosition + (zOrderModifier * (map.zOrderCount + 1))
    # end

    # now actually using the data begins.

    # add layers
    for layerInfo in map.layers
      if layerInfo.visible
        child = TMXLayer.layerWithTilesetInfo map.tilesets, layerInfo: layerInfo, mapInfo: map
        child.zPosition = baseZPosition + ((map.zOrderCount - layerInfo.zOrderCount) * zOrderModifier)

        map.addChild child
      end
    end

    # add tile objects
    for objectGroup in map.objectGroups
      for obj in objectGroup.objects
        num = obj["gid"]
        if num && num.intValue
          tileset = map.tilesetInfoForGid num.intValue
          if tileset  # add a tile object if it is apropriate.
            x = obj["x"]
            y = obj["y"]
            pt = nil

            if map.orientation == ORIENTATION_ISOMETRIC
              # warning these appear to be incorrect for iso maps when used for tile objects!  Unsure why the math is different between objects and regular tiles.
              coords = map.screenCoordToPosition CGPointMake(x, y)
              pt = CGPointMake((map.tileSize.width / 2.0) * (map.tileSize.width + coords.x - coords.y - 1),
                       (map.tileSize.height / 2.0) * (((map.tileSize.height * 2) - coords.x - coords.y) - 2))

              #  NOTE:
              #  iso zPositioning may not work as expected for maps with irregular tile sizes.  For larger tiles (i.e. a box in front of some floor
              #  tiles) We would need each layer to have their tiles ordered lower at the bottom coords and higher at the top coords WITHIN THE LAYER, in
              #  addition to the layers being offset as described below. this could potentially be a lot larger than 20 as a default and may take some
              #  thinking to fix.
            else
              pt = CGPointMake(x + (map.tileSize.width / 2.0), y + (map.tileSize.height / 2.0))
            end
            texture = tileset.textureForGid(num - tileset.firstGid + 1)
            sprite = SKSpriteNode.spriteNodeWithTexture texture
            sprite.position = pt
            sprite.zPosition = baseZPosition + ((map.zOrderCount - objectGroup.zOrderCount) * zOrderModifier)
            sprite.name = obj["name"]
            map.addChild sprite

            #warning This needs to be optimized into tilemap layers like our regular layers above for performance reasons.
            # this could be problematic...  what if a single object group had a bunch of tiles from different tilemaps?  Would this cause zOrder problems if we're adding them all to tilemap layers?
          end
        end
      end
    end

    # add image layers
    for imageLayer in map.imageLayers
      NSLog "painting image layer: #{imageLayer.imageSource}"
      image = SKSpriteNode.spriteNodeWithImageNamed imageLayer.imageSource
      image.position = CGPointMake(image.size.width / 2.0, image.size.height / 2.0)
      image.zPosition = baseZPosition + ((map.zOrderCount - imageLayer.zOrderCount) * zOrderModifier)
      map.addChild image

      #warning the positioning is off here, seems to be bottom-left instead of top-left.  Might be off on the rest of the sprites too...?
    end

    map
  end


  def screenCoordToPosition screenCoord
    CGPoint.new screenCoord.x / tileSize.width, screenCoord.y / tileSize.height
  end

  def tilesetInfoForGid gID
    return nil unless  gID

    for tileset in self.tilesets
      # check to see if the gID is in the info's atlas gID range.  If not, skip this one and go to the next.
      lastPossibleGid = tileset.firstGid + (tileset.atlasTilesPerRow * tileset.atlasTilesPerCol) - 1
      next if (gID < tileset.firstGid) || (gID > lastPossibleGid)

      return tileset;
    end

    nil # should never get here?
  end


  def propertiesForGid gID
    tileProperties[gID]
  end


  def layerNamed name
    for layerInfo in layers
      return layerInfo.layer if name == layerInfo.name
    end
    nil
  end

  def groupNamed name
    for group in objectGroups
      return group if name == group.groupName
    end
    nil
  end

  def init
    super.tap do
      self.currentFirstGID = 0
      self.currentString = ''
      self.storingCharacters = false
      self.layerAttributes = LAYER_ATTRIBUTE_NONE

      self.zOrderCount = 1
      self.parentElement = PROPERTY_NONE;
      self.tilesets = []
      self.tileProperties = {}
      self.properties = {}
      self.layers = []
      self.imageLayers = []
      self.objectGroups = []
      self.resources = nil # possible future resources path
    end
  end

  # the XML parser calls here with all the elements
  def parser parser, didStartElement: elementName, namespaceURI: namespaceURI, qualifiedName: qName, attributes: attributeDict
    if elementName == 'map'
      orientationStr = attributeDict["orientation"]
      if orientationStr.downcase == 'orthogonal'
        self.orientation = ORIENTATION_ORTHOGONAL
      elsif orientationStr.downcase == 'isometric'
        self.orientation = ORIENTATION_ISOMETRIC
      else
        NSLog "Unsupported orientation: #{orientationStr}"
        parser.abortParsing
      end

      self.mapSize = CGSizeMake attributeDict["width"].to_i, attributeDict["height"].to_i
      self.tileSize = CGSizeMake attributeDict["tilewidth"].to_i, attributeDict["tileheight"].to_i

      # The parent element is now "map"
      self.parentElement = PROPERTY_MAP
    elsif elementName == 'tileset'
      # If this has an external tileset we're done
      externalTilesetFilename = attributeDict["source"]
      if externalTilesetFilename
        NSLog "External tilesets unsupported!"
        parser.abortParsing
        return
      end

      gID = nil
      if currentFirstGID == 0
        gID = attributeDict["firstgid"].to_i
      else
        gID = currentFirstGID
        currentFirstGID = 0
      end

      tileset = TMXTilesetInfo.alloc.initWithGid gID, attributes: attributeDict
      tilesets.addObject tileset
    elsif elementName == 'tile'
      unless storingCharacters
        info = tilesets.lastObject
        dict = {}
        self.parentGID = info.firstGid + attributeDict["id"].to_i
        tileProperties[parentGID] = dict

        self.parentElement = PROPERTY_TILE
      else
        self.gidData = [] unless gidData

        # remember XML gids for the data tag in the order they come in.
        gidData.addObject attributeDict["gid"]
      end
    elsif elementName == 'layer'
      layer = TMXLayerInfo.alloc.init
      layer.name = attributeDict["name"]
      layer.layerGridSize = CGSizeMake(attributeDict["width"].to_i, attributeDict["height"].to_i)
      layer.visible = ! (attributeDict["visible"] == '0')
      layer.offset = CGPointMake(attributeDict["x"].to_i, attributeDict["y"].to_i)
      layer.opacity = 1.0
      layer.opacity = attributeDict["opacity"].to_f if attributeDict["opacity"]

      layer.zOrderCount = zOrderCount
      self.zOrderCount += 1

      layers.addObject layer

      self.parentElement = PROPERTY_LAYER
    elsif elementName == 'imagelayer'
      imageLayer = TMXImageLayer.alloc.init
      imageLayer.name = attributeDict["name"]
      imageLayer.zOrderCount = zOrderCount
      self.zOrderCount += 1

      imageLayers.addObject imageLayer

      self.parentElement = PROPERTY_IMAGE_LAYER
    elsif elementName == 'objectgroup'
      objectGroup = TMXObjectGroup.alloc.init
      objectGroup.groupName = attributeDict["name"]

      positionOffset = CGPoint.new
      positionOffset.x = attributeDict["x"].to_i * tileSize.width
      positionOffset.y = attributeDict["y"].to_i * tileSize.height
      objectGroup.positionOffset = positionOffset

      objectGroup.zOrderCount = zOrderCount
      self.zOrderCount += 1

      objectGroups.addObject objectGroup

      # The parent element is now "objectgroup"
      self.parentElement = PROPERTY_OBJECT_GROUP
    elsif elementName == 'image'
      if parentElement == PROPERTY_IMAGE_LAYER
        imageLayer = imageLayers.lastObject
        imageLayer.imageSource = attributeDict["source"]
        # imageLayer.transparencyColor = attributeDict["trans"]
      else
        tileset = tilesets.lastObject

        # build full path
        imageName = attributeDict["source"]
        path = filename.stringByDeletingLastPathComponent
        path = resources unless path
        tileset.setSourceImage path.stringByAppendingPathComponent(imageName)
      end
    elsif elementName == 'data'
      encoding = attributeDict["encoding"]
      compression = attributeDict["compression"]

      self.storingCharacters = true

      if encoding == 'base64'
        layerAttributes |= LAYER_ATTRIBUTE_BASE_64

        if compression == 'gzip'
          layerAttributes |= LAYER_ATTRIBUTE_GZIP
        elsif compression == 'zlib'
          layerAttributes |= LAYER_ATTRIBUTE_ZLIB
        end
      end
    elsif elementName == 'object'
      objectGroup = objectGroups.lastObject

      # The value for "type" was blank or not a valid class name
      # Create an instance of TMXObjectInfo to store the object and its properties
      dict = {}

      # Parse everything automatically
      array = %w(name type width height gid)
      for key in array
        obj = attributeDict[key]
        dict[key] = obj if obj
      end

      # But X and Y since they need special treatment
      # X
      value = attributeDict["x"]
      if value
        x = value.to_i + objectGroup.positionOffset.x
        dict["x"] = x
      end

      # Y
      value = attributeDict["y"]
      if value
        y = value.to_i + objectGroup.positionOffset.y
        # Correct y position. (Tiled's origin is top-left. SpriteKit's origin is bottom-left)
        y = (mapSize.height * tileSize.height) - y - attributeDict["height"].to_i
        dict["y"] = y
      end

      # Add the object to the objectGroup
      objectGroup.objects.addObject dict

      # The parent element is now "object"
      self.parentElement = PROPERTY_OBJECT

    elsif elementName == 'property'
      if parentElement == PROPERTY_NONE
        NSLog "TMX tile map: Parent element is unsupported. Cannot add property named #{attributeDict['name']} with value #{attributeDict['value']}"
      elsif parentElement == PROPERTY_MAP
        # The parent element is the map
        properties[attributeDict["name"]] = attributeDict["value"]
      elsif parentElement == PROPERTY_LAYER
        # The parent element is the last layer
        layer = layers.lastObject
        # Add the property to the layer
        layer.properties[attributeDict["name"]] = attributeDict["value"]
      elsif parentElement == PROPERTY_IMAGE_LAYER
        imageLayer = imageLayers.lastObject
        imageLayer.properties[attributeDict["name"]] = attributeDict["value"]
      elsif parentElement == PROPERTY_OBJECT_GROUP
        # The parent element is the last object group
        objectGroup = objectGroups.lastObject
        objectGroup.properties[attributeDict["name"]] = attributeDict["value"]
      elsif parentElement == PROPERTY_OBJECT
        # The parent element is the last object
        objectGroup = objectGroups.lastObject
        dict = objectGroup.objects.lastObject

        propertyName = attributeDict["name"]
        propertyValue = attributeDict["value"]

        dict[propertyName] = propertyValue
      elsif parentElement == PROPERTY_TILE
        dict = tileProperties[parentGID]
        propertyName = attributeDict["name"]
        propertyValue = attributeDict["value"]
        dict[propertyName] = propertyValue
      end
    elsif elementName == 'polygon'
      # find parent object's dict and add polygon-points to it
      objectGroup = objectGroups.lastObject
      dict = objectGroup.objects.lastObject
      dict["polygonPoints"] = attributeDict["points"]
    elsif elementName == 'polyline'
      # find parent object's dict and add polyline-points to it
      objectGroup = objectGroups.lastObject
      dict = objectGroup.objects.lastObject
      dict["polylinePoints"] = attributeDict["points"]
    elsif elementName == 'ellipse'
      # find parent object's dict and add ellipse to it
      objectGroup = objectGroups.lastObject
      dict = objectGroup.objects.lastObject
      dict.setObject NSNumber.numberWithBool(true), forKey: "ellipse"
    end
  end

  def parser(parser, didEndElement: elementName, namespaceURI: namespaceURI, qualifiedName: qName)
    len = 0

    if elementName == 'data'
      self.storingCharacters = false
      layer = layers.lastObject

      if layerAttributes & LAYER_ATTRIBUTE_BASE_64
        # clean whitespace from string
        self.currentString = NSMutableString.stringWithString currentString.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet)

        decoded = currentString.unpack('m0').first

        if layerAttributes & (LAYER_ATTRIBUTE_GZIP | LAYER_ATTRIBUTE_ZLIB)
          deflated = nil

          deflated = Zlib::Inflate.inflate decoded

          unless deflated
            NSLog "TiledMap: inflate data error"
            parser.abortParsing
            return
          end

          layer.tiles = deflated.unpack("V" * (deflated.length / 4))
        else
          layer.tiles = decoded.unpack("V" * (decoded.length / 4))
        end
      else
        # convert to binary gid data
        if gidData.count
          layer.tiles = []
          for gid in gidData
            layer.tiles << gid.to_i.to_s # probably slow
          end
        end
      end

      gidData.removeAllObjects if gidData
      self.currentString = ''

    elsif elementName == 'map'
      # The map element has ended
      self.parentElement = PROPERTY_NONE
    elsif elementName == 'layer'
      # The layer element has ended
      self.parentElement = PROPERTY_NONE
    elsif elementName == 'objectgroup'
      # The objectgroup element has ended
      self.parentElement = PROPERTY_NONE
    elsif elementName == 'object'
      # The object element has ended
      self.parentElement = PROPERTY_NONE
    end
  end

  def parser parser, foundCharacters: string
    currentString.appendString(string) if storingCharacters
  end

  def parser parser, parseErrorOccurred: parseError
    NSLog "Error on XML Parse: #{parseError.localizedDescription}"
  end
end
