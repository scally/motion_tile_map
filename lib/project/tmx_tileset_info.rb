class TMXTilesetInfo
  # TODO: Duplicated
  TILE_FLAG_DIAGONAL = 0x20000000
  TILE_FLAG_VERTICAL = 0x40000000
  TILE_FLAG_HORIZONTAL = 0x80000000
  TILE_FLAG_FLIPPED_ALL = (TILE_FLAG_HORIZONTAL | TILE_FLAG_VERTICAL | TILE_FLAG_DIAGONAL)
  TILE_FLAG_FLIPPED_MASK = ~TILE_FLAG_FLIPPED_ALL

  attr_accessor :name, :firstGid, :tileSize, :unitTileSize, :imageSize, :spacing, :margin, :sourceImage, :atlasTilesPerRow,
    :atlasTilesPerCol, :atlasTexture, :textureCache

  def initWithGid gID, attributes: attributes
    self.name = attributes['name']
    self.firstGid = gID
    self.spacing = attributes['spacing'] || 0
    self.margin = attributes['margin'] || 0
    self.tileSize = CGSizeMake attributes['tilewidth'], attributes['tileheight']
    self.textureCache = {}

    self
  end

  def setSourceImage sourceImage

    self.sourceImage = sourceImage.copy

    atlas = UIImage.imageWithContentsOfFile sourceImage

    self.imageSize = atlas.size
    # self.atlasTexture = SKTexture.textureWithImage(atlas) # CML: There seems to be a bug where creating with Image instead of ImageNamed breaks the archiving
    self.atlasTexture = SKTexture.textureWithImageNamed sourceImage

    NSLog "texture image: #{sourceImage}, Size (#{atlasTexture.size.width}, #{atlasTexture.size.height})"

    self.unitTileSize = CGSizeMake tileSize.width / imageSize.width, tileSize.height / imageSize.height

    self.atlasTilesPerRow = (imageSize.width - margin * 2 + spacing) / (tileSize.width + spacing)
    self.atlasTilesPerCol = (imageSize.height - margin * 2 + spacing) / (tileSize.height + spacing)
  end

  def rowFromGid gid
    (gid / atlasTilesPerRow).to_i
  end

  def colFromGid gid
    gid % atlasTilesPerRow
  end

  def textureForGid gid
    gid = gid & TILE_FLAG_FLIPPED_MASK
    gid -= firstGid

    texture = textureCache[gid]
    unless texture
      rowOffset = (((tileSize.height + spacing) * rowFromGid(gid)) + margin) / imageSize.height
      colOffset = (((tileSize.width + spacing) * colFromGid(gid)) + margin) / imageSize.width
      # reverse y axis
      rowOffset = 1.0 - rowOffset - unitTileSize.height

      # note that the width and height of the tiles are always the same in TMX maps or the atlas (GIDs) couldn't be calculated consistently.
      rect = CGRectMake colOffset, rowOffset, unitTileSize.width, unitTileSize.height
      texture = SKTexture.textureWithRect rect, inTexture: atlasTexture
      texture.usesMipmaps = true
      texture.filteringMode = SKTextureFilteringNearest
      textureCache[gid] = texture
    end

    texture
  end

  def textureAtPoint p
    atlas = atlasTexture
    SKTexture.textureWithRect CGRectMake(p.x / atlas.size.width, 1.0-((p.y + tileSize.height) / atlas.size.height),
      unitTileSize.width, unitTileSize.height), inTexture: atlas
  end

end
