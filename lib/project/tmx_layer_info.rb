class TMXLayerInfo
  attr_accessor :properties, :name, :layerGridSize, :tiles, :visible, :opacity, :minGID, :maxGID, :offset, :layer, :zOrderCount

  def init
    super

    self.properties = {}

    self
  end

  def tileGidAtCoord coord
    idx = coord.x + coord.y * layerGridSize.width

    NSAssert idx < (layerGridSize.width * layerGridSize.height), "index out of bounds!"

    tiles[idx]
  end
end
