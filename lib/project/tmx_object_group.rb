class TMXObjectGroup
  attr_accessor :groupName, :positionOffset, :objects, :properties, :zOrderCount

  def init
    self.objects = []
    self.properties = {}

    self
  end

  def objectNamed objectName
    objects.find { |object| object.valueForKey('name') == objectName }
  end

  def objectsNamed objectName
    objects.select { |object| object.valueForKey('name') == objectName }
  end

  def propertyNamed propertyName
    properties[propertyName]
  end
end
