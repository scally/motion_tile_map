class TMXLayer < SKNode
  attr_accessor :layerInfo, :tileInfo, :mapTileSize, :map

  ORIENTATION_ORTHOGONAL = 0
  ORIENTATION_ISOMETRIC = 1

  # TODO: Duplicated
  TILE_FLAG_DIAGONAL = 0x20000000
  TILE_FLAG_VERTICAL = 0x40000000
  TILE_FLAG_HORIZONTAL = 0x80000000
  TILE_FLAG_FLIPPED_ALL = (TILE_FLAG_HORIZONTAL | TILE_FLAG_VERTICAL | TILE_FLAG_DIAGONAL)
  TILE_FLAG_FLIPPED_MASK = ~TILE_FLAG_FLIPPED_ALL

  def pointForCoord coord
    CGPointMake coord.x * mapTileSize.width + mapTileSize.width / 2,
      layerHeight - (coord.y * mapTileSize.height + mapTileSize.height / 2)
  end

  def coordForPoint inPoint
    inPoint.y = layerHeight - inPoint.y
    x = inPoint.x / mapTileSize.height
    y = inPoint.y / mapTileSize.width

    CGPointMake x, y
  end

  def tileGidAt point
    pt = coordForPoint point
    idx = pt.x + pt.y * layerInfo.layerGridSize.width

    # bounds check, invalid GID if out of bounds
    if (idx > layerInfo.layerGridSize.width * layerInfo.layerGridSize.height) || idx < 0
      NSAssert true, "index out of bounds!"
      return 0
    end

    # return the Gid
    layerInfo.tiles[idx]
  end


  def tileAt point
    tileAtCoord coordForPoint(point)
  end

  def tileAtCoord coord
    nodeName = (coord.x + coord.y * layerInfo.layerGridSize.width).to_s
    childNodeWithName nodeName
  end

  def setTileGidAt coord, gid
    #TODO: need to write setTileGidAt:
    raise 'Not Implemented Yet'
  end

  def removeTileAtCoord coord
    gid = layerInfo.tileGidAtCoord coord

    if gid
      z = coord.x + coord.y * layerInfo.layerGridSize.width

      # remove tile from GID map
      layerInfo.tiles[z] = 0

      tileNode = childNodeWithName (coord.x + coord.y * self.layerInfo.layerGridSize.width).to_s
      tileNode.removeFromParent if tileNode
    end
  end

  def properties
    layerInfo.properties
  end

  def propertyWithName name
    layerInfo.properties[name]
  end

  def self.layerWithTilesetInfo tilesets, layerInfo: layerInfo, mapInfo: mapInfo

    layer = TMXLayer.node
    layer.map = mapInfo

    # basic properties from layerInfo
    layer.tileInfo = []
    layer.layerInfo = layerInfo
    layer.layerInfo.layer = layer
    layer.mapTileSize = mapInfo.tileSize
    layer.alpha = layerInfo.opacity
    layer.position = layerInfo.offset

    # recalc the offset if we are isometriic
    if mapInfo.orientation == ORIENTATION_ISOMETRIC
      layer.position = CGPointMake((layer.mapTileSize.width / 2.0) * (layer.position.x - layer.position.y),
        (layer.mapTileSize.height / 2.0) * (-layer.position.x - layer.position.y))
    end

    layerNodes = {}

    # loop through the tiles
    for col in 0..layerInfo.layerGridSize.width
      for row in 0..layerInfo.layerGridSize.height
        # get the gID
        gID = layerInfo.tiles[col + (row * layerInfo.layerGridSize.width)]

        # mask off the flip bits and remember their result.
        flipX = (gID & TILE_FLAG_HORIZONTAL) != 0
        flipY = (gID & TILE_FLAG_VERTICAL) != 0
        flipDiag = (gID & TILE_FLAG_DIAGONAL) != 0
        gID = gID & TILE_FLAG_FLIPPED_MASK

        # skip 0 GIDs
        next unless gID

        # get the tileset for the passed gID.  This will allow us to support multiple tilesets!
        tilesetInfo = mapInfo.tilesetInfoForGid gID
        layer.tileInfo.addObject tilesetInfo

        if tilesetInfo
          texture = tilesetInfo.textureForGid gID
          sprite = SKSpriteNode.spriteNodeWithTexture texture
          sprite.name = (col + row * layerInfo.layerGridSize.width).to_s

          # make sure it's in the right position.
          if mapInfo.orientation == ORIENTATION_ISOMETRIC
            sprite.position = CGPointMake((layer.mapTileSize.width / 2.0) * (layerInfo.layerGridSize.width + col - row - 1),
              (layer.mapTileSize.height / 2.0) * ((layerInfo.layerGridSize.height * 2 - col - row) - 2) )
          else
            sprite.position = CGPointMake(col * layer.mapTileSize.width + tilesetInfo.tileSize.width/2.0,
              (mapInfo.mapSize.height * (layer.mapTileSize.height)) - ((row + 1) * layer.mapTileSize.height) + tilesetInfo.tileSize.height/2.0)
          end

          # flip sprites if necessary
          if flipDiag
            if flipX
              sprite.zRotation = -M_PI_2
            elsif flipY
              sprite.zRotation = M_PI_2
            end
          else
            if flipY
              sprite.yScale *= -1
            end
            if flipX
              sprite.xScale *= -1
            end
          end

          # add sprite to correct node for this tileset
          layerNode = layerNodes[tilesetInfo.name]
          unless layerNode
            layerNode = SKNode.alloc.init
            layerNodes[tilesetInfo.name] = layerNode
          end
          layerNode.addChild sprite
        end
      end
    end

    # add nodes for any tilesets that were used in this layer
    for layerNode in layerNodes.allValues
      layer.addChild(layerNode) if layerNode.children.count > 0
    end

    layer.calculateAccumulatedFrame

    layer
  end

  def layerWidth
    layerInfo.layerGridSize.width * mapTileSize.width
  end

  def layerHeight
    layerInfo.layerGridSize.height * mapTileSize.height
  end
end

